#include "Controller.hpp"

Controller::Controller(dart::dynamics::SkeletonPtr _robot, dart::simulation::WorldPtr _world)
	: mRobot(_robot), mWorld(_world)
{
    setInitialConfiguration();

    // some useful pointers to robot limbs
    mLeftFoot = mRobot->getBodyNode("l_sole");
    mRightFoot = mRobot->getBodyNode("r_sole");
    mBase = mRobot->getBodyNode("base_link");
    mTorso = mRobot->getBodyNode("body");

    // Initialize walk state
    walkState.iter = 0;
    walkState.iter = 0;
    walkState.footstepCounter = 0;
    walkState.supportFoot = false;

    // Plan footsteps

    std::vector<Vref> vrefSequence;

    for (int i = 0; i < 5000; i++) {
        if (i < 100) vrefSequence.push_back(Vref(0.0, 0.0, 0.00));
        else vrefSequence.push_back(Vref(0.2, 0.0, 0.00));
    }

    bool firstSupportFootIsLeft = false;
    Eigen::VectorXd leftFootPose(6), rightFootPose(6);
    leftFootPose << getRPY(mLeftFoot), mLeftFoot->getCOM();
    rightFootPose << getRPY(mRightFoot), mRightFoot->getCOM();
    
    footstepPlan = new FootstepPlan;
    footstepPlan->plan(vrefSequence, leftFootPose, rightFootPose, firstSupportFootIsLeft);

    // Retrieve current state

    current.comPos = mRobot->getCOM();
    current.comVel = mRobot->getCOMLinearVelocity();
    current.comAcc = mRobot->getCOMLinearAcceleration();
    current.zmpPos = current.comPos - current.comAcc / (omega*omega);
    current.leftFootPos = mLeftFoot->getCOM();
    current.rightFootPos = mRightFoot->getCOM();
    current.torsoOrient = getRPY(mBase);
    current.leftFootOrient = getRPY(mLeftFoot);
    current.rightFootOrient = getRPY(mRightFoot);

    // Initialize desired state with reasonable values

    desired.comPos = Eigen::Vector3d(current.comPos(0), current.comPos(1), comTargetHeight);
    desired.comVel = Eigen::Vector3d::Zero();
    desired.zmpPos = Eigen::Vector3d(current.comPos(0), current.comPos(1),0.0);
    desired.leftFootPos = Eigen::Vector3d(current.comPos(0), 0.08, 0.0);
    desired.rightFootPos = Eigen::Vector3d(current.comPos(0), -0.08, 0.0);
    desired.torsoOrient = Eigen::Vector3d(0.0, 0.0, 0.0);
    desired.leftFootOrient = Eigen::Vector3d(0.0, 0.0, 0.0);
    desired.rightFootOrient = Eigen::Vector3d(0.0, 0.0, 0.0);
    desired.comAcc = omega*omega * (desired.comPos - desired.zmpPos);

    // Create file loggers

    logList.push_back(new Logger("desired.comPos", &desired.comPos));
    logList.push_back(new Logger("desired.comVel", &desired.comVel));
    logList.push_back(new Logger("desired.zmpPos", &desired.zmpPos));

    logList.push_back(new Logger("current.comPos", &current.comPos));
    logList.push_back(new Logger("current.comVel", &current.comVel));
    logList.push_back(new Logger("current.zmpPos", &current.zmpPos));

    // Instantiate MPC solver

    solver = new MPC(footstepPlan);
}

Controller::~Controller() {}

void Controller::update() {
    // This adds a push to the robot
    //if (mWorld->getSimFrames()>=510 && mWorld->getSimFrames()<=520) mBase->addExtForce(Eigen::Vector3d(0,300,0));

    if (mWorld->getSimFrames()==500) system("gnuplot ../plotters/plot");
    walkState.simulationTime = mWorld->getSimFrames();

    // Retrieve current state in the world frame

    current.comPos = mRobot->getCOM();
    current.comVel = mRobot->getCOMLinearVelocity();
    current.comAcc = mRobot->getCOMLinearAcceleration();

    current.zmpPos = current.comPos - current.comAcc / (omega*omega);
    current.leftFootPos = mLeftFoot->getCOM();
    current.rightFootPos = mRightFoot->getCOM();
    current.torsoOrient = getRPY(mBase);
    current.leftFootOrient = getRPY(mLeftFoot);
    current.rightFootOrient = getRPY(mRightFoot);

    // Use info from current state to update MPC state

    State stateForMpc = desired;
    
    if (walkState.iter % 10 == 0 && walkState.footstepCounter > 1) {

        stateForMpc.comPos(0) += 0.125 * (current.comPos(0) - desired.comPos(0));
        stateForMpc.comVel(0) += 0.125 * (current.comVel(0) - desired.comVel(0));
        stateForMpc.comPos(1) += 0.125 * (current.comPos(1) - desired.comPos(1));
        stateForMpc.comVel(1) += 0.125 * (current.comVel(1) - desired.comVel(1));

        stateForMpc.leftFootPos += 0.01 * (current.leftFootPos - desired.leftFootPos);
        stateForMpc.rightFootPos += 0.01 * (current.rightFootPos - desired.rightFootPos);
    }

    // Compute the new desired state using MPC

    desired = solver->solve(desired, walkState);
 
    // Compute inverse kinematics

    Eigen::VectorXd qDot =  getJointVelocitiesStacked(desired, current, walkState);

    // Set the velocity of the floating base to zero
    for (int i = 0; i < 6; ++i) {
        mRobot->setCommand(i, 0);
    }

    // Set the velocity of each joint as per inverse kinematics
    for (int i = 0; i < 50; ++i) {
        mRobot->setCommand(i+6,qDot(i));
    }

    // Store the results in files (for plotting)
    for (int i = 0; i < logList.size(); ++i) {
        logList.at(i)->log();
    }

    // Arm swing
    mRobot->setPosition(mRobot->getDof("R_SHOULDER_P")->getIndexInSkeleton(), (4+5*sin(2*M_PI*0.01*(mWorld->getSimFrames())))*M_PI/180);
    mRobot->setPosition(mRobot->getDof("L_SHOULDER_P")->getIndexInSkeleton(), (4-5*sin(2*M_PI*0.01*(mWorld->getSimFrames())))*M_PI/180);

    // Update the iteration counters, if a step is finished reset and change support foot
    ++walkState.iter;

    if (footstepPlan->getFootstepStartTiming(walkState.footstepCounter) + walkState.iter >= footstepPlan->getFootstepEndTiming(walkState.footstepCounter)) {
    	walkState.iter = 0;
        walkState.footstepCounter++;
	walkState.supportFoot = !walkState.supportFoot;
        //std::cout << "Iteration " << walkState.iter << " Footstep " << walkState.footstepCounter << std::endl;
    }
}

Eigen::MatrixXd Controller::getTorsoAndSwfJacobian() { //FIXME this is still expressed in relative frame
    if (walkState.supportFoot == true) {
        mSupportFoot = mRobot->getBodyNode("r_sole");
        mSwingFoot = mRobot->getBodyNode("l_sole");
    } else {
        mSupportFoot = mRobot->getBodyNode("l_sole");
        mSwingFoot = mRobot->getBodyNode("r_sole");
    }

    Eigen::MatrixXd Jacobian_supportToBase;

    Jacobian_supportToBase =  (mRobot->getCOMJacobian(mSupportFoot) - mRobot->getJacobian(mSupportFoot,mSupportFoot));

    for (unsigned int i=0; i<44; i++){ 
    if (i!=5 || i!=6)  Jacobian_supportToBase.col(i).setZero();
    }

    Eigen::MatrixXd Jacobian_SupportToSwing = mRobot->getJacobian(mSwingFoot,mSupportFoot) - mRobot->getJacobian(mSupportFoot,mSupportFoot);

    Eigen::MatrixXd Jacobian_tot_(12, 56);

    Jacobian_tot_ << Jacobian_supportToBase, Jacobian_SupportToSwing;

    Eigen::MatrixXd Jacobian_tot(12, 50);

    // Remove the floating base columns
    Jacobian_tot = Jacobian_tot_.block<12,50>(0, 6);

    return Jacobian_tot;
}

Eigen::VectorXd Controller::getJointVelocitiesQp(State current, State desired) {
    int nVariables = 50;

    double jointVelocitiesGain = 0.00001;//0.00001;
    Eigen::MatrixXd taskGain = Eigen::MatrixXd::Identity(12,12);

    // Torso Orientation
    taskGain(0,0) = 1;
    taskGain(1,1) = 1;
    taskGain(2,2) = 1;

    // CoM Position
    taskGain(3,3) = 5;
    taskGain(4,4) = 5;
    taskGain(5,5) = 1;

    // Swing Foot Orientation
    taskGain(6,6) = 5;
    taskGain(7,7) = 5;
    taskGain(8,8) = 1;

    // Swing Foot Position
    taskGain(9,9) = 5;
    taskGain(10,10) = 5;
    taskGain(11,11) = 5;

    // Construct stack of error, wrap to Pi angular errors
    Eigen::VectorXd desired_pos(12);
    desired_pos << desired.getRelComPose(walkState.supportFoot), desired.getRelSwingFootPose(walkState.supportFoot);

    Eigen::VectorXd actual_pos(12);
    actual_pos << current.getRelComPose(walkState.supportFoot), current.getRelSwingFootPose(walkState.supportFoot);


    Eigen::VectorXd errorStack = actual_pos - desired_pos;
    errorStack(0) = wrapToPi(errorStack(0));
    errorStack(1) = wrapToPi(errorStack(1));
    errorStack(2) = wrapToPi(errorStack(2));
    errorStack(6) = wrapToPi(errorStack(6));
    errorStack(7) = wrapToPi(errorStack(7));
    errorStack(8) = wrapToPi(errorStack(8));

    // Cost Function
    Eigen::MatrixXd Jacobian_tot = getTorsoAndSwfJacobian();
    Eigen::MatrixXd costFunctionH = mWorld->getTimeStep()*mWorld->getTimeStep()*Jacobian_tot.transpose()*taskGain*Jacobian_tot +
			jointVelocitiesGain*Eigen::MatrixXd::Identity(nVariables,nVariables);

    Eigen::VectorXd costFunctionF = mWorld->getTimeStep()*Jacobian_tot.transpose() * taskGain * IKerrorGain * errorStack;

    // Constraint RHipYawPitch and LHipYawPitch to be at the same angle
    // not working on HRP4
    Eigen::MatrixXd AHip = Eigen::MatrixXd::Zero(1,nVariables);
    AHip(0,52-6) = -mWorld->getTimeStep();
    AHip(0,46-6) = mWorld->getTimeStep();
    Eigen::VectorXd bHip(1);
    bHip(0) = mRobot->getPosition(54-6) - mRobot->getPosition(48-6);

    // Solve the QP
    Eigen::VectorXd solution = solveQP(costFunctionH, costFunctionF, 0*AHip, 0*bHip, 0*bHip);

    return solution;
}

Eigen::VectorXd Controller::getJointVelocitiesStacked(State desired, State current, WalkState walkState) {

    Eigen::VectorXd feedforward = Eigen::VectorXd::Zero(12);
    feedforward << desired.getRelComVelocity(walkState.supportFoot), desired.getRelSwingFootVelocity(walkState.supportFoot);
     
    Eigen::VectorXd desired_pos(12);
    desired_pos << desired.getRelComPose(walkState.supportFoot), desired.getRelSwingFootPose(walkState.supportFoot);

    Eigen::VectorXd current_pos(12);
    current_pos << current.getRelComPose(walkState.supportFoot), current.getRelSwingFootPose(walkState.supportFoot);

    // Get the proper jacobian and pseudoinvert it
    Eigen::MatrixXd Jacobian_tot = getTorsoAndSwfJacobian();
    Eigen::MatrixXd PseudoJacobian_tot = (Jacobian_tot.transpose())*(Jacobian_tot*Jacobian_tot.transpose()).inverse();

    Eigen::MatrixXd _taskGain = Eigen::MatrixXd::Identity(12,12);

    // Torso Orientation
    _taskGain(0,0) = 0.1;
    _taskGain(1,1) = 0.1;
    _taskGain(2,2) = 0.1;

    // CoM Position
    _taskGain(3,3) = 5;
    _taskGain(4,4) = 5;
    _taskGain(5,5) = 1;

    // Swing Foot Orientation
    _taskGain(6,6) = 1;
    _taskGain(7,7) = 1;
    _taskGain(8,8) = 1;

    // Swing Foot Position
    _taskGain(9,9) = 5;
    _taskGain(10,10) = 5;
    _taskGain(11,11) = 5;

    double ikGain = 10;

    Eigen::VectorXd qDot = PseudoJacobian_tot* (feedforward + ikGain*_taskGain*(desired_pos - current_pos));

    return qDot;
}

Eigen::Vector3d Controller::getRPY(dart::dynamics::BodyNode* body, dart::dynamics::BodyNode* referenceFrame) {
    Eigen::MatrixXd rotMatrix = body->getTransform(referenceFrame).rotation();

    Eigen::Vector3d RPY;
    RPY << atan2(rotMatrix(2,1),rotMatrix(2,2)),
        atan2(-rotMatrix(2,0),sqrt(rotMatrix(2,1)*rotMatrix(2,1)+rotMatrix(2,2)*rotMatrix(2,2))),
        atan2(rotMatrix(1,0),rotMatrix(0,0));

    return RPY;
}

Eigen::Vector3d Controller::getRPY(dart::dynamics::BodyNode* body) {
    Eigen::MatrixXd rotMatrix = body->getTransform().rotation();

    Eigen::Vector3d RPY;
    RPY << atan2(rotMatrix(2,1),rotMatrix(2,2)),
        atan2(-rotMatrix(2,0),sqrt(rotMatrix(2,1)*rotMatrix(2,1)+rotMatrix(2,2)*rotMatrix(2,2))),
        atan2(rotMatrix(1,0),rotMatrix(0,0));

    return RPY;
}

Eigen::Vector3d Controller::getZmpFromExternalForces()
{
    Eigen::Vector3d zmp_v;
    bool left_contact = false;
    bool right_contact = false;

    Eigen::Vector3d left_cop;
    if(abs(mLeftFoot->getConstraintImpulse()[5]) > 0.01){
        left_cop << -mLeftFoot->getConstraintImpulse()(1)/mLeftFoot->getConstraintImpulse()(5), mLeftFoot->getConstraintImpulse()(0)/mLeftFoot->getConstraintImpulse()(5), 0.0;
        Eigen::Matrix3d iRotation = mLeftFoot->getWorldTransform().rotation();
        Eigen::Vector3d iTransl   = mLeftFoot->getWorldTransform().translation();
        left_cop = iTransl + iRotation*left_cop;
        left_contact = true;
    }

    Eigen::Vector3d right_cop;
    if(abs(mRightFoot->getConstraintImpulse()[5]) > 0.01){
        right_cop << -mRightFoot->getConstraintImpulse()(1)/mRightFoot->getConstraintImpulse()(5), mRightFoot->getConstraintImpulse()(0)/mRightFoot->getConstraintImpulse()(5), 0.0;
        Eigen::Matrix3d iRotation = mRightFoot->getWorldTransform().rotation();
        Eigen::Vector3d iTransl   = mRightFoot->getWorldTransform().translation();
        right_cop = iTransl + iRotation*right_cop;
        right_contact = true;
    }

    if(left_contact && right_contact){
        zmp_v << (left_cop(0)*mLeftFoot->getConstraintImpulse()[5] + right_cop(0)*mRightFoot->getConstraintImpulse()[5])/(mLeftFoot->getConstraintImpulse()[5] + mRightFoot->getConstraintImpulse()[5]),
                 (left_cop(1)*mLeftFoot->getConstraintImpulse()[5] + right_cop(1)*mRightFoot->getConstraintImpulse()[5])/(mLeftFoot->getConstraintImpulse()[5] + mRightFoot->getConstraintImpulse()[5]),
		 0.0;
    }else if(left_contact){
        zmp_v << left_cop(0), left_cop(1), 0.0;
    }else if(right_contact){
        zmp_v << right_cop(0), right_cop(1), 0.0;
    }else{
        // No contact detected
        zmp_v << 0.0, 0.0, 0.0;
    }

    return zmp_v;
}

void Controller::setInitialConfiguration() {
    Eigen::VectorXd q = mRobot->getPositions();

    // Floating Base
    q[0] = 0.0;
    q[1] = 4*M_PI/180.0;
    q[2] = 0.0;
    q[3] = 0.00;
    q[4] = 0.00;
    q[5] = 0.753;

    // Right Leg
    q[44] = 0.0;           // hip yaw
    q[45] = 3*M_PI/180;    // hip roll
    q[46] = -25*M_PI/180;  // hip pitch
    q[47] = 50*M_PI/180;   // knee pitch
    q[48] = -30*M_PI/180;  // ankle pitch
    q[49] = -4*M_PI/180;   // ankle roll         
    // Left Leg
    q[50] = 0.0;           // hip yaw
    q[51] = -3*M_PI/180;   // hip roll
    q[52] = -25*M_PI/180;  // hip pitch
    q[53] = 50*M_PI/180;   // knee pitch
    q[54] = -30*M_PI/180;  // ankle pitch
    q[55] = 4*M_PI/180;    // ankle roll       

    mRobot->setPositions(q);

    // Additional arm position setting
    mRobot->setPosition(mRobot->getDof("R_SHOULDER_P")->getIndexInSkeleton(), (4)*M_PI/180 );
    mRobot->setPosition(mRobot->getDof("R_SHOULDER_R")->getIndexInSkeleton(), -8*M_PI/180  );
    mRobot->setPosition(mRobot->getDof("R_SHOULDER_Y")->getIndexInSkeleton(), 0 );

    mRobot->setPosition(mRobot->getDof("R_ELBOW_P")->getIndexInSkeleton(), -25*M_PI/180 );
    mRobot->setPosition(mRobot->getDof("R_ELBOW_P")->getIndexInSkeleton(), -25*M_PI/180 );

    mRobot->setPosition(mRobot->getDof("L_SHOULDER_P")->getIndexInSkeleton(), (4)*M_PI/180  );
    mRobot->setPosition(mRobot->getDof("L_SHOULDER_R")->getIndexInSkeleton(), 8*M_PI/180  );
    mRobot->setPosition(mRobot->getDof("L_SHOULDER_Y")->getIndexInSkeleton(), 0 );

    mRobot->setPosition(mRobot->getDof("L_ELBOW_P")->getIndexInSkeleton(), -25*M_PI/180 ); 
    mRobot->setPosition(mRobot->getDof("L_ELBOW_P")->getIndexInSkeleton(), -25*M_PI/180 ); 
}
