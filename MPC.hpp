#pragma once

#include <Eigen/Core>
#include "qpOASES/qpOASES.hpp"
#include "types.hpp"
#include "parameters.cpp"
#include "utils.cpp"
#include <vector>
#include <iostream>
#include <fstream>
#include "FootstepPlan.hpp"
#include <chrono>

class MPC{
    public:
    MPC(FootstepPlan* _footstepPlann);
    ~MPC();

    // Compute the next desired state starting from the current state
    State solve(State current, WalkState walkState);

    private:
    // Matrices for prediction
    Eigen::VectorXd p;
    Eigen::MatrixXd P;
    Eigen::MatrixXd Vu;
    Eigen::MatrixXd Vs;

    // Matrices for cost function
    Eigen::MatrixXd costFunctionH = Eigen::MatrixXd::Zero(2*(N+M),2*(N+M));
    Eigen::VectorXd costFunctionF = Eigen::VectorXd::Zero(2*(N+M));

    // Matrices for stability constraint
    Eigen::MatrixXd Aeq = Eigen::MatrixXd::Zero(2,(N*2)+(M*2));
    Eigen::VectorXd beq = Eigen::VectorXd::Zero(2);

    //Matrices for balance constraint
    Eigen::MatrixXd AZmp = Eigen::MatrixXd::Zero(2*N,2*(N+M));
    Eigen::VectorXd bZmpMax = Eigen::VectorXd::Zero(2*N);
    Eigen::VectorXd bZmpMin = Eigen::VectorXd::Zero(2*N);

    // Matrices for feasibility constraints
    Eigen::MatrixXd AFootsteps = Eigen::MatrixXd::Zero(2*M,2*(M+N));
    Eigen::VectorXd bFootstepsMax = Eigen::VectorXd::Zero(2*M);
    Eigen::VectorXd bFootstepsMin = Eigen::VectorXd::Zero(2*M);

    // Matrices for the stacked constraints
    Eigen::MatrixXd AConstraint;
    Eigen::VectorXd bConstraintMax;
    Eigen::VectorXd bConstraintMin;

    // Footstep plan
    FootstepPlan* footstepPlan;

    // Midpoint of ZMP constraint
    Eigen::VectorXd x_midpoint, y_midpoint;
};
