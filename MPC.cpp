#include "MPC.hpp"

MPC::MPC(FootstepPlan* _footstepPlan) : footstepPlan(_footstepPlan) {

    // Matrices for ZMP prediction
    p = Eigen::VectorXd::Ones(N);
    P = Eigen::MatrixXd::Zero(N,N);

    for(int i = 0; i < N; ++i)
    	for(int j = 0; j <= i; ++j)
            P(i, j) = timeStep;
}

MPC::~MPC() {}

State MPC::solve(State current, WalkState walkState) {

    // Reset constraint matrices
    AZmp.setZero();
    AFootsteps.setZero();

    // Get the pose of the support foot in the world frame
    Eigen::VectorXd supportFootPose = current.getSupportFootPose(walkState.supportFoot);

    // Construct some matrices that will be used later in the cost function and constraints
    // ************************************************************************************

    // Construct the Cc matrix, which maps iterations to predicted/previewed footsteps
    Eigen::MatrixXd CcFull = Eigen::MatrixXd::Zero(N+prev,2*M+1);

    int fsAhead = 0;
    for (int i = 0; i < N + prev; ++i) { // we iterate over control + preview
        // if with the prediction index i we have reach the next footstep increase fsAhead
        if (footstepPlan->getFootstepStartTiming(walkState.footstepCounter) + walkState.iter + i + 1 >= footstepPlan->getFootstepEndTiming(walkState.footstepCounter + fsAhead)) fsAhead++;
        
        int samplesTillNextFootstep = footstepPlan->getFootstepEndTiming(walkState.footstepCounter + fsAhead) 
				      - (footstepPlan->getFootstepStartTiming(walkState.footstepCounter) + walkState.iter + i + 1);
        
        if (samplesTillNextFootstep > doubleSupportSamples) {
            CcFull(i, fsAhead) = 1;
        } else {
            CcFull(i, fsAhead) = (double)samplesTillNextFootstep / (double)doubleSupportSamples;
            CcFull(i, fsAhead + 1) = 1.0 - (double)samplesTillNextFootstep / (double)doubleSupportSamples;
        }
    }

    // Split the matrix: currentFootstepZmp is for the current, Cc for all predicted footsteps
    Eigen::VectorXd currentFootstepZmp = CcFull.block(0,0,N,1);
    Eigen::MatrixXd Cc = CcFull.block(0,1,N,M);

    // Construct the Ic matrix, which removes constraints from double support phases
    Eigen::MatrixXd Ic = Eigen::MatrixXd::Identity(N,N);
    for (int i = 0; i < N; ++i) {
        if (walkState.footstepCounter == 0 && walkState.iter+i <= footstepPlan->getFootstepEndTiming(0)) Ic(i,i) = 0;
    }

    // Construct the difference matrix (x_j - x_{j-1})
    Eigen::MatrixXd differenceMatrix = Eigen::MatrixXd::Identity(M,M);
    for (int i = 0; i < M-1; ++i) {
        differenceMatrix(i+1,i) = -1;
    }

    // Retrieve footstep orientations and positions over preview/control horizon
    Eigen::VectorXd previewOrientations(2*M+1);
    Eigen::VectorXd previewPositions_x(2*M+1);
    Eigen::VectorXd previewPositions_y(2*M+1);

    previewOrientations(0) = supportFootPose(2);
    previewPositions_x(0) = supportFootPose(3);
    previewPositions_y(0) = supportFootPose(4);

    for (int i = 1; i < 2*M+1; i++) {
        previewOrientations(i) = footstepPlan->getFootstepOrientation(walkState.footstepCounter + i - 1);
        previewPositions_x(i) = footstepPlan->getFootstepPosition(walkState.footstepCounter + i - 1)(0);
        previewPositions_y(i) = footstepPlan->getFootstepPosition(walkState.footstepCounter + i - 1)(1);
    }

    Eigen::VectorXd predictedOrientations = previewOrientations.head(M+1);
    Eigen::VectorXd predictedPositions_x = previewPositions_x.head(M+1);
    Eigen::VectorXd predictedPositions_y = previewPositions_y.head(M+1);

    // Construct the stability constraint
    // **********************************

    Eigen::Vector3d anticipativeTail = Eigen::Vector3d::Zero();

    Eigen::VectorXd previewMidpoint_x = CcFull * previewPositions_x;
    Eigen::VectorXd previewMidpoint_y = CcFull * previewPositions_y;

    for (int i = 0; i < prev; i++) {
        anticipativeTail(0) += exp(-omega*timeStep*(N + i)) * (1 - exp(-omega*timeStep)) * previewMidpoint_x(N+i);
        anticipativeTail(1) += exp(-omega*timeStep*(N + i)) * (1 - exp(-omega*timeStep)) * previewMidpoint_y(N+i);
    }

    anticipativeTail(0) += exp(-omega*timeStep*(N + prev)) * previewMidpoint_x(N+prev-1);
    anticipativeTail(1) += exp(-omega*timeStep*(N + prev)) * previewMidpoint_y(N+prev-1);

    double stabConstrMultiplier = (1-exp(-omega*timeStep)) / omega;
    for(int i = 0; i < N; ++i) {
        Aeq(0,i)     = stabConstrMultiplier * exp(-omega*timeStep*i) - timeStep * exp(-omega*timeStep*N);
        Aeq(1,N+M+i) = stabConstrMultiplier * exp(-omega*timeStep*i) - timeStep * exp(-omega*timeStep*N);
    }

    beq << current.comPos(0) + current.comVel(0)/omega - current.zmpPos(0) * (1.0 - exp(-omega*timeStep*N)) - anticipativeTail(0),
           current.comPos(1) + current.comVel(1)/omega - current.zmpPos(1) * (1.0 - exp(-omega*timeStep*N)) - anticipativeTail(1);

    // Construct the ZMP constraint
    // ****************************

    // Construct the ZMP rotation matrix
    Eigen::VectorXd thetaAlongPreview = CcFull * previewOrientations;

    Eigen::MatrixXd rCosZmp = Eigen::MatrixXd::Identity(N,N);
    Eigen::MatrixXd rSinZmp = Eigen::MatrixXd::Zero(N,N);
    for (int i = 0; i < N; ++i) {
        rCosZmp(i,i) = cos(thetaAlongPreview(i));
        rSinZmp(i,i) = sin(thetaAlongPreview(i));
    }

    Eigen::MatrixXd zmpRotationMatrix(2*N,2*N);
    zmpRotationMatrix <<  rCosZmp,rSinZmp,
		         -rSinZmp,rCosZmp;

    // Construct the A matrix of the ZMP constraint, by diagonalizing two of the same, then rotating
    Eigen::MatrixXd halfAZmp(N,N+M);
    halfAZmp << Ic*P, -Ic*Cc;
    AZmp.block(0,0,N,N+M) = halfAZmp;
    AZmp.block(N,N+M,N,N+M) = halfAZmp;

    AZmp = zmpRotationMatrix * AZmp;

    // Construct the b vector of the ZMP constraint
    Eigen::VectorXd bZmpSizeTerm = Eigen::VectorXd::Zero(2*N);
    Eigen::VectorXd bZmpStateTerm = Eigen::VectorXd::Zero(2*N);

    bZmpSizeTerm << Ic*p*footConstraintSquareWidth/2, Ic*p*footConstraintSquareWidth/2;

    bZmpStateTerm << Ic*(-p*current.zmpPos(0)+currentFootstepZmp*supportFootPose(3)), Ic*(-p*current.zmpPos(1)+currentFootstepZmp*supportFootPose(4));
    bZmpStateTerm = zmpRotationMatrix * bZmpStateTerm;

    bZmpMin = - bZmpSizeTerm + bZmpStateTerm;
    bZmpMax =   bZmpSizeTerm + bZmpStateTerm;

    // Construct the kinematic constraint
    // **********************************

    // Construct the matrices that activate the left or right constraint
    Eigen::VectorXd pFr = Eigen::VectorXd::Zero(M);
    Eigen::VectorXd pFl = Eigen::VectorXd::Zero(M);
    Eigen::VectorXd pF = Eigen::VectorXd::Ones(M);
    for (int i = 0; i < M; ++i) {
        pFr(i) = (int)(walkState.supportFoot + i) % 2;
    }
    pFl = pF - pFr;

    // Vector for the current footstep position
    Eigen::VectorXd currentFootstepKinematic = Eigen::VectorXd::Zero(M);
    currentFootstepKinematic(0) = 1;

    // Construct the kinematic rotation matrix
    Eigen::MatrixXd footstepsRotationMatrix(2*M,2*M);
    Eigen::MatrixXd rCosFootsteps = Eigen::MatrixXd::Identity(M,M);
    Eigen::MatrixXd rSinFootsteps = Eigen::MatrixXd::Zero(M,M);
    for(int i=0; i<M; ++i){
        rCosFootsteps(i,i) = cos(predictedOrientations(i));
        rSinFootsteps(i,i) = sin(predictedOrientations(i));
    }
    footstepsRotationMatrix <<  rCosFootsteps, rSinFootsteps,
			       -rSinFootsteps, rCosFootsteps;

    // Assemble the A matrix for the kinematic constraint, and rotate
    AFootsteps.block(0,N,M,M) = differenceMatrix;
    AFootsteps.block(M,2*N+M,M,M) = differenceMatrix;
    AFootsteps = footstepsRotationMatrix * AFootsteps;

    // Assemble the b vector for the kinematic constraint
    bFootstepsMin << -pF*deltaXMax + supportFootPose(3)*currentFootstepKinematic, -pFl*deltaYOut + pFr*deltaYIn  + supportFootPose(4)*currentFootstepKinematic;
    bFootstepsMax <<  pF*deltaXMax + supportFootPose(3)*currentFootstepKinematic, -pFl*deltaYIn  + pFr*deltaYOut + supportFootPose(4)*currentFootstepKinematic;

    // Construct the cost function
    // ***************************

    // Construct the H matrix, which is made of two of the same halfH block
    Eigen::MatrixXd halfH = Eigen::MatrixXd::Zero(N+M, N+M);
    halfH << qZd*Eigen::MatrixXd::Identity(N,N) + qZ*P.transpose()*P,
             -qZ*P.transpose()*Cc, -qZ*Cc.transpose()*P, qZ*Cc.transpose()*Cc + qF*Eigen::MatrixXd::Identity(M,M);
    costFunctionH.block(0,0,N+M,N+M) = halfH;
    costFunctionH.block(N+M,N+M,N+M,N+M) = halfH;

    // Contruct candidate footstep vectors
    Eigen::VectorXd xCandidateFootsteps(M), yCandidateFootsteps(M);
    for (int i = 0; i < M; i++) {
        yCandidateFootsteps(i) = footstepPlan->getFootstepPosition(walkState.footstepCounter + i)(1);
        xCandidateFootsteps(i) = footstepPlan->getFootstepPosition(walkState.footstepCounter + i)(0);
    }

    // Construct the F vector
    Eigen::Vector3d stateX = Eigen::Vector3d(current.comPos(0), current.comVel(0), current.zmpPos(0));
    Eigen::Vector3d stateY = Eigen::Vector3d(current.comPos(1), current.comVel(1), current.zmpPos(1));

    costFunctionF << qZ*P.transpose()*(p*current.zmpPos(0) - currentFootstepZmp*supportFootPose(3)),
		     -qZ*Cc.transpose()*(p*current.zmpPos(0) - currentFootstepZmp*supportFootPose(3)) - qF*xCandidateFootsteps,
		     qZ*P.transpose()*(p*current.zmpPos(1) - currentFootstepZmp*supportFootPose(4)),
		     -qZ*Cc.transpose()*(p*current.zmpPos(1) - currentFootstepZmp*supportFootPose(4)) - qF*yCandidateFootsteps;

    // Stack the constraint matrices, stability constrait goes first!
    // **************************************************************

    int nConstraints = Aeq.rows() + AFootsteps.rows() + AZmp.rows();
    AConstraint.resize(nConstraints, 2*(N+M));
    bConstraintMin.resize(nConstraints);
    bConstraintMax.resize(nConstraints);

    AConstraint    << Aeq, AFootsteps, AZmp;
    bConstraintMin << beq, bFootstepsMin, bZmpMin;
    bConstraintMax << beq, bFootstepsMax, bZmpMax;

    // Solve QP and update state
    // *************************

    Eigen::VectorXd decisionVariables = solveQP_hpipm(costFunctionH, costFunctionF, AConstraint, bConstraintMin, bConstraintMax);
    
    // Split the QP solution in ZMP dot and footsteps
    Eigen::VectorXd zDotOptimalX = decisionVariables.head(N);
    Eigen::VectorXd zDotOptimalY = decisionVariables.segment(N+M,N);
    Eigen::VectorXd footstepsOptimalX = decisionVariables.segment(N,M);
    Eigen::VectorXd footstepsOptimalY = decisionVariables.segment(2*N+M,M);

    // Update the com-torso state based on the result of the QP
    double ch = cosh(omega*timeStep);
    double sh = sinh(omega*timeStep);

    Eigen::Matrix3d A_upd = Eigen::MatrixXd::Zero(3,3);
    Eigen::Vector3d B_upd = Eigen::VectorXd::Zero(3);
    A_upd<<ch,sh/omega,1-ch,omega*sh,ch,-omega*sh,0,0,1;
    B_upd<<timeStep-sh/omega,1-ch,timeStep;

    Eigen::Vector3d currentStateX = Eigen::Vector3d(current.comPos(0), current.comVel(0), current.zmpPos(0));
    Eigen::Vector3d currentStateY = Eigen::Vector3d(current.comPos(1), current.comVel(1), current.zmpPos(1));
    Eigen::Vector3d nextStateX = A_upd*currentStateX + B_upd*zDotOptimalX(0);
    Eigen::Vector3d nextStateY = A_upd*currentStateY + B_upd*zDotOptimalY(0);

    State next = current;
    next.comPos = Eigen::Vector3d(nextStateX(0), nextStateY(0), comTargetHeight);
    next.comVel = Eigen::Vector3d(nextStateX(1), nextStateY(1), 0.0);
    next.zmpPos = Eigen::Vector3d(nextStateX(2), nextStateY(2), 0.0);
    next.comAcc = omega*omega * (next.comPos - next.zmpPos);
    next.torsoOrient = Eigen::Vector3d(0.0, 0.0, 0.0);

    Eigen::Vector4d footstepPredicted;
    footstepPredicted << footstepsOptimalX(0),footstepsOptimalY(0),0.0,predictedOrientations(1);

    // Update swing foot position
    
    // If it's the first step, or we are in double support, keep the foot on the ground
    int timeSinceFootstepStart = walkState.iter;
    int singleSupportDuration = footstepPlan->getFootstepDuration(walkState.footstepCounter) - doubleSupportSamples;
    int timeNextFootstep = footstepPlan->getFootstepDuration(walkState.footstepCounter) - walkState.iter;
    bool isDoubleSupport = timeNextFootstep <= doubleSupportSamples;

    double swingFootHeight = -(4*stepHeight/pow((double)singleSupportDuration,2)) * timeSinceFootstepStart * (timeSinceFootstepStart - singleSupportDuration);
    double swingFootVerticalVelocity = -(4*stepHeight/pow((double)singleSupportDuration,2)) * (2 * timeSinceFootstepStart - singleSupportDuration);
    
    if (walkState.footstepCounter == 0 || isDoubleSupport) swingFootHeight = 0.0;

    // If support foot is left, move right foot
    if (walkState.supportFoot == 0) {
        if (isDoubleSupport){
            next.rightFootPos = current.rightFootPos;
            next.rightFootPos(2) = swingFootHeight;
            next.rightFootVel = Eigen::Vector3d::Zero();
        } else {
	    next.rightFootPos += kSwingFoot * (footstepPredicted.head(3) - current.rightFootPos);
            next.rightFootPos(2) = swingFootHeight;
            next.rightFootVel = kSwingFoot * (footstepPredicted.head(3) - current.rightFootPos) / timeStep;
            next.rightFootVel(2) = swingFootVerticalVelocity;
        }
        
        next.rightFootAcc = Eigen::Vector3d::Zero();
        next.rightFootOrient(2) += 5 * timeSinceFootstepStart * kSwingFoot * wrapToPi(footstepPredicted(3) - current.rightFootOrient(2));
    } else {
        if (isDoubleSupport){
            next.leftFootPos = current.leftFootPos;
            next.leftFootPos(2) = swingFootHeight;
            next.leftFootVel = Eigen::Vector3d::Zero();
        } else {
	    next.leftFootPos += kSwingFoot * (footstepPredicted.head(3) - current.leftFootPos);
            next.leftFootPos(2) = swingFootHeight;
            next.leftFootVel = kSwingFoot * (footstepPredicted.head(3) - current.leftFootPos) / timeStep;
            next.leftFootVel(2) = swingFootVerticalVelocity;
        }
        
        next.leftFootAcc = Eigen::Vector3d::Zero();
        next.leftFootOrient(2) += 5 * timeSinceFootstepStart * kSwingFoot * wrapToPi(footstepPredicted(3) - current.leftFootOrient(2));
    }

    next.torsoOrient(2) = wrapToPi((next.leftFootOrient(2) + next.rightFootOrient(2)) / 2.0);

    // Return next robot state
    return next;
}
