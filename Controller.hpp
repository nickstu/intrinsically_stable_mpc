#pragma once

#include <Eigen/Core>
#include "dart/dart.hpp"
#include "MPC.hpp"
#include  <fstream>
#include "utils.cpp"
#include "types.hpp"
#include "FootstepPlan.hpp"
#include <string>

class Controller
{
public:
  Controller(dart::dynamics::SkeletonPtr _robot, dart::simulation::WorldPtr _world);
  virtual ~Controller();

  Eigen::Vector3d getZmpFromExternalForces();

  void update();

  Eigen::MatrixXd getTorsoAndSwfJacobian();

  Eigen::VectorXd getJointVelocitiesQp(State current, State desired);
  Eigen::VectorXd getJointVelocitiesQpAcceleration(State current, State desired);

  void setInitialConfiguration();

  Eigen::VectorXd getJointVelocitiesStacked(State desired, State current, WalkState walkState);

  Eigen::Vector3d getRPY(dart::dynamics::BodyNode*, dart::dynamics::BodyNode*);
  Eigen::Vector3d getRPY(dart::dynamics::BodyNode*);

private:
  dart::dynamics::SkeletonPtr mRobot;

  dart::dynamics::BodyNode* mTorso;

  dart::simulation::WorldPtr mWorld;

  MPC* solver;

  dart::dynamics::BodyNode* mLeftFoot;
  dart::dynamics::BodyNode* mRightFoot;
  dart::dynamics::BodyNode* mSupportFoot;
  dart::dynamics::BodyNode* mSwingFoot;
  dart::dynamics::BodyNode* mBase;

  State desired;
  State current;
  WalkState walkState;

  FootstepPlan* footstepPlan;

  std::vector<Logger*> logList;

public:

};
