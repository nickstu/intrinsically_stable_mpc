#pragma once

#include <Eigen/Core>
#include "utils.cpp"

// Contains the state of the LIP robot
struct State {
    Eigen::Vector3d comPos;
    Eigen::Vector3d comVel;
    Eigen::Vector3d comAcc;
    Eigen::Vector3d zmpPos;

    Eigen::Vector3d leftFootPos;
    Eigen::Vector3d leftFootVel;
    Eigen::Vector3d leftFootAcc;

    Eigen::Vector3d rightFootPos;
    Eigen::Vector3d rightFootVel;
    Eigen::Vector3d rightFootAcc;

    Eigen::Vector3d torsoOrient;
    Eigen::Vector3d leftFootOrient;
    Eigen::Vector3d rightFootOrient;

    inline Eigen::VectorXd getComPose() {
	Eigen::VectorXd comPose(6);
        comPose << torsoOrient, comPos;
        return comPose;
    }

    inline Eigen::VectorXd getSupportFootPose(bool supportFoot) {
	Eigen::VectorXd sfPose(6);
        if (supportFoot == 0) sfPose << leftFootOrient, leftFootPos;
        else sfPose << rightFootOrient, rightFootPos;
        return sfPose;
    }

    inline Eigen::VectorXd getSupportFootOrientation(bool supportFoot) {
	Eigen::VectorXd sfPose(6);
        if (supportFoot == 0) sfPose << leftFootOrient, Eigen::Vector3d::Zero();
        else sfPose << rightFootOrient, Eigen::Vector3d::Zero();
        return sfPose;
    }

    inline Eigen::VectorXd getSwingFootPose(bool supportFoot) {
	Eigen::VectorXd sfPose(6);
        if (supportFoot == 1) sfPose << leftFootOrient, leftFootPos;
        else sfPose << rightFootOrient, rightFootPos;
        return sfPose;
    }

    inline Eigen::VectorXd getComVelocity() {
	Eigen::VectorXd comVelocity(6);
        comVelocity << Eigen::Vector3d::Zero(), comVel; //FIXME not zero
        return comVelocity;
    }

    inline Eigen::VectorXd getSwingFootVelocity(bool supportFoot) {
	Eigen::VectorXd sfVelocity(6);
        if (supportFoot == 1) sfVelocity << Eigen::Vector3d::Zero(), leftFootVel;
        else sfVelocity << Eigen::Vector3d::Zero(), rightFootVel; //FIXME not zero
        return sfVelocity;
    }

    inline Eigen::VectorXd getRelComPose(bool supportFoot) {
	return vvRel(getComPose(), getSupportFootPose(supportFoot));
    }

    inline Eigen::VectorXd getRelSwingFootPose(bool supportFoot) {
	return vvRel(getSwingFootPose(supportFoot), getSupportFootPose(supportFoot));
    }

    inline Eigen::VectorXd getRelComVelocity(bool supportFoot) {
	return vvRel(getComVelocity(), getSupportFootOrientation(supportFoot));
    }

    inline Eigen::VectorXd getRelSwingFootVelocity(bool supportFoot) {
	return vvRel(getSwingFootVelocity(supportFoot), getSupportFootOrientation(supportFoot));
    }

    inline void print() {
        std::cout << "com position: " << comPos;
        std::cout << "left foot position: " << leftFootPos;
        std::cout << "right foot position: " << rightFootPos;
    }
};

struct WalkState {
    bool supportFoot;
    double simulationTime;
    int iter, footstepCounter, indInitial;
};

struct Vref {
    Vref(double _x, double _y, double _omega) : x(_x), y(_y), omega(_omega) {}

    double x = 0;
    double y = 0;
    double omega = 0;
}; 
